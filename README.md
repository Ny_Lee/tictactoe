# Tic-Tac-Toe
This project generates 3 by 3 Tic-Tac-Toe board, randomly distributes O's and X's on the board, then returns the result where which player won or tie.

---

## Prerequisites
You will need Visual Studio 2017 to run this program on your device.

---

## Running the tests
Open the project with Visual Studio and hit Run button to run the program.

---

## Authors
Ny Lee - Initial work

---

## License
This project is licensed under the MIT License - see the LICENSE.md file for details