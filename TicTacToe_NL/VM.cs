﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

//Group 6: Joe Huang, Ny Lee, Sonam Sonam, Chaitany Virothi, Harpreet Kaur
namespace TicTacToe_NL
{
    class VM : INotifyPropertyChanged
    {
        // 8 possible solutions in a 3D array:
        readonly int[,,] SOLUTION = new int[8, 3, 3] { { {1, 1, 1},
                                                         {0, 0, 0},
                                                         {0, 0, 0} },
                                                       { {1, 0, 0},
                                                         {0, 1, 0},
                                                         {0, 0, 1} },
                                                       { {1, 0, 0},
                                                         {1, 0, 0},
                                                         {1, 0, 0} },
                                                       { {0, 1, 0},
                                                         {0, 1, 0},
                                                         {0, 1, 0} },
                                                       { {0, 0, 1},
                                                         {0, 0, 1},
                                                         {0, 0, 1} },
                                                       { {0, 0, 1},
                                                         {0, 1, 0},
                                                         {1, 0, 0} },
                                                       { {0, 0, 0},
                                                         {1, 1, 1},
                                                         {0, 0, 0} },
                                                       { {0, 0, 0},
                                                         {0, 0, 0},
                                                         {1, 1, 1} } };
        private string row0Col0 = "";
        public string Row0Col0
        {
            get => row0Col0;
            set { row0Col0 = value; NotifyChanged(); }
        }
        private string row0Col1 = "";
        public string Row0Col1
        {
            get => row0Col1;
            set { row0Col1 = value; NotifyChanged(); }
        }
        private string row0Col2 = "";
        public string Row0Col2
        {
            get => row0Col2;
            set { row0Col2 = value; NotifyChanged(); }
        }
        private string row1Col0 = "";
        public string Row1Col0
        {
            get => row1Col0;
            set { row1Col0 = value; NotifyChanged(); }
        }
        private string row1Col1 = "";
        public string Row1Col1
        {
            get => row1Col1;
            set { row1Col1 = value; NotifyChanged(); }
        }
        private string row1Col2 = "";
        public string Row1Col2
        {
            get => row1Col2;
            set { row1Col2 = value; NotifyChanged(); }
        }
        private string row2Col0 = "";
        public string Row2Col0
        {
            get => row2Col0;
            set { row2Col0 = value; NotifyChanged(); }
        }
        private string row2Col1 = "";
        public string Row2Col1
        {
            get => row2Col1;
            set { row2Col1 = value; NotifyChanged(); }
        }
        private string row2Col2 = "";
        public string Row2Col2
        {
            get => row2Col2;
            set { row2Col2 = value; NotifyChanged(); }
        }
        private string winner = "";
        public string Winner
        {
            get => winner;
            set { winner = value; NotifyChanged(); }
        }
        private static int[,] RandomArrayGenerator()
        {
            Random rnd = new Random();
            int[,] array = new int[3, 3];
            int countO = 0;
            int countX = 0;
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    // make sure each player gets to play every other turn
                    int OorX = rnd.Next(0, 2);
                    if (OorX == 0)
                        countO++;
                    if (countO > 5 && OorX == 0)
                        OorX = 1;
                    if (OorX == 1)
                        countX++;
                    if (countX > 5 && OorX == 1)
                        OorX = 0;
                    array[i, j] = OorX;
                }
            return array;
        }
        private void Display()
        {
            int[,] array = RandomArrayGenerator();
            Row0Col0 = (array[0, 0] == 0) ? "O" : "X";
            Row0Col1 = (array[0, 1] == 0) ? "O" : "X";
            Row0Col2 = (array[0, 2] == 0) ? "O" : "X";
            Row1Col0 = (array[1, 0] == 0) ? "O" : "X";
            Row1Col1 = (array[1, 1] == 0) ? "O" : "X";
            Row1Col2 = (array[1, 2] == 0) ? "O" : "X";
            Row2Col0 = (array[2, 0] == 0) ? "O" : "X";
            Row2Col1 = (array[2, 1] == 0) ? "O" : "X";
            Row2Col2 = (array[2, 2] == 0) ? "O" : "X";
        }
        public void NewGame()
        {
            int[,] array = RandomArrayGenerator();
            int winnerO = 0;
            int winnerX = 0;
            List<int> intList = new List<int>();
            // compare array with 8 possible solutions:
            while (true)
            {
                for (int i = 0; i < 8; i++)
                {
                    for (int j = 0; j < 3; j++)
                        for (int k = 0; k < 3; k++)
                            if (SOLUTION[i, j, k] == 1)
                                intList.Add(array[j, k]);
                    if (intList[0] == intList[1] && intList[0] == intList[2])
                        if (intList[0] == 0)
                            winnerO++;
                        else
                            winnerX++;
                    intList.Clear();
                }
                if (winnerO == 1 && winnerX == 0)
                    break;
                else if (winnerO == 0 && winnerX == 1)
                    break;
                else if (winnerO == 0 && winnerX == 0)
                    break;
                else
                {
                    winnerO = 0;
                    winnerX = 0;
                    array = RandomArrayGenerator();
                }
            }
            Display();
            if (winnerO == winnerX)
                Winner = "No winner, play again!";
            else
                Winner = (winnerO > winnerX) ? "Player O won!" : "Player X won!";
        }
        #region
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
